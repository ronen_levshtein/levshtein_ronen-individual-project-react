import React, { useState, useEffect} from "react";
import Img1 from './images/Img-2.jpg';
import Img2 from './images/Img-3.jpg';
import { FaGithubSquare } from 'react-icons/fa'

const Portfolio = () => {

    const [record,setRecord] = useState([]);
    const [token, setToken] = useState(localStorage.getItem('token'));

    const checkToken = async () => {
        fetch('http://localhost:3001/user/token', {
            method: "POST",
            headers: {
                'x-access-token': localStorage.getItem('token')
            }})
            .then(function(response){
                return response.json();
            })
            .then(function(myJson) {
                setToken(myJson.token);
                // setRole(myJson.role);
            });
    }

    useEffect(() => {
        checkToken();
    }, [token])

    // On Page load display all records 
    const loadPortfolioDetails = async () =>  
    {
        fetch('http://localhost:3001/portfoliolist')
            .then(function(response){
                return response.json();
            })
            .then(function(myJson) {
                setRecord(myJson);
            });
    }
    useEffect(() => {
        loadPortfolioDetails();
    }, []);

    return (
        <>
            {token && 
                <main className="portfolioContainer">
                    <div className="info-title">
                        <h1>Portfolio</h1>
                    </div>
                    <div className="portfolio">
                        {record.slice(0).reverse().map((name, key)=> {
                            return (
                                <div key={key} className="optionsMain">
                                    <div className="options">
                                        <h2>Title:</h2> <br/>
                                        <h4>{name.title}</h4>
                                        <h2>Description:</h2> <br/>
                                        <h4>{name.description}</h4>
                                        <p><img style={{ width: '100%', height: 200 }} src={name.image} alt="img" /></p>
                                        <a href={name.git} target='_blank' rel="noreferrer">
                                            <FaGithubSquare />
                                            Git Link
                                        </a>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </main>
            }
            {!token &&
                <main className="portfolioContainer">
                    <div className="info-title">
                        <h1>Portfolio</h1>
                    </div>
                    <div className="portfolio">
                        <div className="optionsMain">
                            <div className="options">
                                <h1>First Project</h1>
                                <h1>HTML </h1>
                                <p><img style={{ width: 250, height: 200 }} src={Img1} alt="img" /></p>
                            </div>
                        </div>
                        <div className="optionsMain">
                            <div className="options">
                                <h1>Second Project</h1>
                                <h1>CSS </h1>
                                <p><img style={{ width: 250, height: 200 }} src={Img2} alt="img" /></p>
                            </div>
                        </div>
                    </div>
                </main>
            }
        </>
    )
}

export default Portfolio
