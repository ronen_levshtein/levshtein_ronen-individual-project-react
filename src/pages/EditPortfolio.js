import React, { useState, useEffect, useLayoutEffect } from "react";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import FileBase64 from 'react-file-base64';
import { FaTimes } from 'react-icons/fa';

const EditPortfolio = () => {

  useLayoutEffect(() => {
    window.scrollTo(0, 1)
  }, []);

  let history = useHistory(); //The useHistory hook gives you access to the history instance that you may use to navigate.
  const { id } = useParams();  //The useParams() hook helps us to access the URL parameters from a current route. 
  

  const [user ,setUser] = useState({
      title:"",
      description:"",
      image: "",
      git: "",
  })
  
  
  const { title, description, image, git } = user;
  
  const onInputChange = e => {
    setUser({ ...user,[e.target.name]: e.target.value });
  };

  const onImageChange = (base64) => {
    setUser({ ...user,image: base64 });
  }
  
  const loadUser =  () => {
    axios.get(`http://localhost:3001/portfolio/${id}`,{
            method: "GET",
          })
            .then((response) => {
              setUser({
                    id: response.data._id,
                    update: true,
                    title: response.data.title,
                    description: response.data.description,
                    image: response.data.image,
                    git: response.data.git,
                }); 
            }).catch((error) => console.log("error", error));
  };
  
  useEffect(() => {
    loadUser();
  }, []);

  const updatePortfolio = e => {
    e.preventDefault();
    axios.put(`http://localhost:3001/editedportfolio/${id}`, user);
    alert('Portfolio Updated');
    history.push("/AdminPortfolio");
    
  };


  return (
    <div className="container editP editPortfolio">
      <div className="row mt-4"> 
        <div className="col-sm-7 col-offset-3 mx-auto shadow p-5">
          <div className="closeBtn">
            <FaTimes 
              className='closeEdit'
              onClick={() => history.push('/AdminPortfolio')}
            />
          </div>
          <h4 className="text-center mb-4">Edit Portfolio</h4>
          <h5 className="text-success">Portfolio ID : {user.id} </h5>
          <div className="form-group mb-3">
            <input
              type="text"
              className="form-control form-control-lg"
              name="title"
              value={title}
              onChange={e => onInputChange(e)}
            />
          </div>
          <div className="form-group mb-3">
            <input
              type="text"
              className="form-control form-control-lg"
              name="description"
              value={description}
              onChange={e => onInputChange(e)}
            />
          </div>
          <div className="form-group mb-3">
            <input
              type="text"
              className="form-control form-control-lg"
              name="git"
              placeholder="Add Git Link"
              value={git}
              onChange={e => onInputChange(e)}
            />
          </div>
          <div className="form-group">
            <FileBase64
              type="file"
              multiple={false}
              onDone={({ base64 }) => onImageChange(base64)}
            />
          </div>
          <br/>
          <div className="form-group mb-3">
            <img className="activator" style={{ width: '100%', height: 300 }} src={image} alt="img" />
          </div>
          <button 
            onClick={updatePortfolio} 
            className="btn btn-secondary btn-block"
          >
            Update Portfolio
          </button>

        </div>
      </div> 
    </div>
  );
};

export default EditPortfolio;