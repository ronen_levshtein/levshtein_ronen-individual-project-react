import React from 'react';

const Home = () => {

    return (
        <main className='homeMain'>
                <div className="home">
                    <section className="info">
                        <h1 className="description">Full Stack Web Developer</h1>
                        <p>Register for more Web Samples with Git Links
                            <br />
                            <br />
                            After much game and practice, I was able to offer more options and updates to this website.
                            After passing the Login page, which also has design and Java applied to it, this page offers links to other pages,
                            and design platforms.
                            Here you will find drop down menu, gallery and a built-in resume page, as well as a How To video.
                            Enjoy browsing through the site and feel free to use the contact page to challenge my creativity.
                        </p>
                    </section>
                    <div className='quote'>
                        <p className="text-white m-0 test">Don't go where click may link, but instead, create a new route and leave a path!</p>
                    </div>
                </div>
        </main>
    );
};

export default Home;