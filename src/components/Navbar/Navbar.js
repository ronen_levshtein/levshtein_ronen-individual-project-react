import React, { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom'
import './Navbar.css';
import { BsFillPeopleFill } from 'react-icons/bs';
import { FaHome } from 'react-icons/fa';
import { FaBars } from 'react-icons/fa';
import { FaTimes } from 'react-icons/fa';
import { createBrowserHistory } from "history";

function Navbar() {

    const [click, setClick] = useState(false);
    const [colorChange, setColorchange] = useState(false);
    const [token, setToken] = useState(localStorage.getItem('token'));
    const [role, setRole] = useState('');
    const [id, setId] = useState();
    const [user, setUser] = useState('')

    const withRefresh = createBrowserHistory({ forceRefresh: true });

    const changeNavbarColor = () =>{
        if(window.scrollY >= 20) {
        setColorchange(true);
        }
        else{
        setColorchange(false);
        }
    };
    window.addEventListener('scroll', changeNavbarColor);

    const handleClick = () => setClick(!click);

    const closeMobileMenu = () => setClick(false);

    const checkToken = async () => {
        fetch('http://localhost:3001/user/token', {
            method: "POST",
            headers: {
                'x-access-token': localStorage.getItem('token')
            }
        })
        .then(function(response){
            console.log(response.status)
            return response.json();
        })
        .then(function(myJson) {
            console.log(myJson)
            if(myJson.message === 'jwt expired') {
                localStorage.removeItem('token')
                localStorage.removeItem('id')
                setToken('')
                setRole('')
                setUser('')
                withRefresh.push('/home')
            } else {
                setUser(myJson)
                setId(myJson.id);
                setToken(myJson.token);
                setRole(myJson.role);
            }
        });
    }

    useEffect(() => {
        checkToken();
    }, [user])

    const handleLogOut = () => {
        localStorage.removeItem('token')
        localStorage.removeItem('id')
        setToken('')
        setRole('')
        setUser('')
        withRefresh.push('/login')
    }

    console.log(user)

    return (
    <>
        <nav className={colorChange ? 'navbar colorChange' : 'navbar'}>
                <div className='menu-icon' onClick={handleClick}>
                    {click ? <FaTimes className='fa-times' /> : <FaBars className='fa-bars' />}
                </div>
                <ul className={click ? 'nav-menu active' : 'nav-menu'}>
                    <li className='nav-item'>
                        <NavLink to='/home' 
                            className='nav-links' 
                            activeStyle={{background: '#1888ff', color: 'white', borderRadius: '4px'}} 
                            onClick={() => {
                                closeMobileMenu();
                            }}
                        >
                            <FaHome />
                            Home
                        </NavLink>
                    </li>
                    <li className='nav-item'>
                        <NavLink to='/about' 
                            className='nav-links' 
                            activeStyle={{background: '#1888ff', color: 'white', borderRadius: '4px'}} 
                            onClick={() => {
                                closeMobileMenu();
                            }}
                        >
                            <BsFillPeopleFill />
                            About Me
                        </NavLink>
                    </li>
                    <li className='nav-item'>
                        <NavLink to='/Portfolio' 
                            className='nav-links' 
                            activeStyle={{background: '#1888ff', color: 'white', borderRadius: '4px'}}  
                            onClick={() => {
                                closeMobileMenu();
                            }}
                        >
                            <i className='fas fa-caret-down' /> 
                            Portfolio
                        </NavLink>
                    </li>
                    {token &&
                        <li className='nav-item'>
                            <NavLink to='/Contact'
                                className='nav-links'
                                id={id}
                                activeStyle={{background: '#1888ff', color: 'white', borderRadius: '4px'}}  
                                onClick={() => {
                                    closeMobileMenu();
                                }}
                            >
                                <i className='fas fa-caret-down' /> 
                                Contact
                            </NavLink>
                        </li>
                    }
                    {token && 
                        <li className='nav-item'>
                            <NavLink to='/personalinfo' 
                                className='nav-links' 
                                activeStyle={{background: '#1888ff', color: 'white', borderRadius: '4px'}} 
                                onClick={() => {
                                    closeMobileMenu();
                                }}
                            >
                                <i className='fas fa-caret-down' /> 
                                Personal Info
                            </NavLink>
                        </li>
                    }
                    {role === 'admin' &&
                        <>
                            <li className='nav-item'>
                                <NavLink to='/AdminPortfolio' 
                                    className='nav-links' 
                                    activeStyle={{background: '#1888ff', color: 'white', borderRadius: '4px'}} 
                                    onClick={() => {
                                        closeMobileMenu();
                                    }}
                                >
                                    <i className='fas fa-caret-down' /> 
                                    Admin Portfolio
                                </NavLink>
                            </li>
                            <li className='nav-item'>
                                <NavLink to='/Users' 
                                    className='nav-links' 
                                    activeStyle={{background: '#1888ff', color: 'white', borderRadius: '4px'}} 
                                    onClick={() => {
                                        closeMobileMenu();
                                    }}
                                >
                                    <i className='fas fa-caret-down' /> 
                                    Users
                                </NavLink>
                            </li>
                            <li className='nav-item'>
                                <NavLink to='/AdminContact' className='nav-links' onClick={closeMobileMenu}>
                                    <i className='fas fa-caret-down' /> 
                                    Admin Contact
                                </NavLink>
                            </li>
                        </>
                    }
                    {!token && (
                        <li className='nav-item'>
                            <NavLink to='/login' className='nav-links' activeStyle={{background: '#1888ff', color: 'white', borderRadius: '4px'}} onClick={closeMobileMenu}>
                                Login
                            </NavLink>
                        </li>
                    )}
                    {token && (
                        <li className='nav-item'>
                            <NavLink to='/login' className='nav-links' onClick={handleLogOut}>
                                Log Out
                            </NavLink>
                        </li>
                    )}
                </ul>
            </nav>
    </>
    )
}

export default Navbar