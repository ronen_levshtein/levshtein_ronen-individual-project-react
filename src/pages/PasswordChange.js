import React, { useState } from 'react';
import { FaTimes } from 'react-icons/fa';

function PasswordChange(props) {

    console.log(props.email)
    const [currentPassword, setCurrentPassword] = useState();
    const [newPassword, setNewPassword] = useState();
    const [confirmNewPassword, setConfirmNewPassword] = useState();

    const submit = async () => {
        if (confirmNewPassword === newPassword) {
            await fetch("http://localhost:3001/user/passwordChange", {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    id: props.id,
                    email: props.email,
                    currentPassword,
                    newPassword,
                })
            }).then((data) => {
                if (data.status === 200){
                    alert('Password was Updated')
                    props.setTrigger(false)
                }
            })
        } else {
            alert("Passwords DO NOT MATCH")
        }
    }

    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            e.preventDefault();
            submit();
        }
    }

    return (props.trigger) ? (
        <div className='passChange'>
            <FaTimes 
					className='passChangeClose'
					onClick={() => props.setTrigger(false)}
			/>
            <div className='passChangediv'>
                <div className='passChangeTitle'>
                    <h1>Change Password</h1>
                </div>
                <div className='passChangeInput'>
                    <input 
                        placeholder='Current Password'
                        type='password'
                        onChange={(e) => setCurrentPassword(e.target.value)}
                    />
                    <input 
                        placeholder='New Password'
                        type='password'
                        onChange={(e) => setNewPassword(e.target.value)}
                        // onKeyDown={handleKeyDown}
                    />
                    <input 
                        placeholder='Confirm New Password'
                        type='password'
                        onChange={(e) => setConfirmNewPassword(e.target.value)}
                        onKeyDown={handleKeyDown}
                    />
                    <button 
						className="btnsubmit" 
                        type='password'
						onClick={submit}
					> 
						Submit
					</button>
                </div>
            </div>
            {props.children}    
        </div>
    ) : "";
}

export default PasswordChange