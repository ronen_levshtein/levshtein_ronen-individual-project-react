import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, useEffect} from "react";
import axios from "axios";
import { useHistory } from 'react-router-dom';
import { FaTrash } from 'react-icons/fa';
import { FaEdit } from 'react-icons/fa';
import { Container, Table } from 'react-bootstrap';
import AddPortfolio from './AddPortfolio';

function PortfolioDetails() {

const [search,setSearch] =useState('');
const [record,setRecord] = useState([]);
const [ buttonPopup, setButtonPopup ] = useState(false);
  
  const history = useHistory();

  // On Page load display all records 
  const loadPortfolioDetails = async () =>  
  {
    fetch('http://localhost:3001/portfoliolist')
          .then(function(response){
            return response.json();
          })
          .then(function(myJson) {
            setRecord(myJson);
          });
    }
    useEffect(() => {
      if(search === "") {
        loadPortfolioDetails();
      }
    }, [search, record]);

    // Delete portfolio Record
    const deleteRecord = (id) =>
    {
      axios.delete(`http://localhost:3001/deleteportfolio/${id}`)
        loadPortfolioDetails();
    };

  // Search Records here 
  useEffect(() => {
    const searchRecords = async () =>
    {
            await axios.get(`http://localhost:3001/portfoliosearch/${search}`)
            .then((response) => {
                if(response.data === 'No Such Record') {
                    alert('No Such Record')
                    loadPortfolioDetails()
                } else {
                    setRecord(response.data);
                }
            });
    }
    if (search === "") {
        loadPortfolioDetails()
    } else {
        searchRecords()
    }
}, [search])

  return(
    <>
      <AddPortfolio 
        trigger={buttonPopup}
        setTrigger={setButtonPopup}
      />
      <div className={buttonPopup ? 'adminport colorChange' : 'adminport'}>
        <h1 className="title1">ADMIN PORTFOLIO</h1>
        <div className="viewPortfolio">
          <h3 className="">View Records</h3>
          <div className="addRecord">
            <div className='searchRecord'>
              <div className="">
                <input 
                  type="text" 
                  id="form1" 
                  onChange={(e)=>setSearch(e.target.value)} 
                  className="userSearch" 
                  placeholder="Search Portfolio Here" 
                  style={{backgroundColor:"#ececec"}}
                />
              </div>
            </div>
            <div className='addRecordBtn'>
              <button 
                className='addRecordBtn1'
                onClick={() => {
                  setButtonPopup(true);
                }}
              >Add New Record</button>
            </div>
          </div>
        </div>
        <Container responsive className='contain1'>
          <Table responsive bordered className='noWrap'>
            <thead>
              <tr>
                <th>#</th>
                <th>Title</th>
                <th>Description</th>
                <th>Image</th>
                <th>Git Link</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {record.map((name, key)=>
                <tr className="active" key={key}>
                  <td>{key + 1}</td>
                  <td>{name.title}</td>
                  <td>{name.description}</td>
                  <td><img className="activator" style={{ width: '80%', height: 80 }} src={name.image} alt="img" /></td>
                  <td>{name.git}</td>
                  <td>
                    <FaTrash 
                      className="text-danger"
                      onClick={() => {
                        const confirmBox = window.confirm(
                          "Do you really want to delete "+ name.title
                        )
                        if (confirmBox === true) {
                          deleteRecord(name._id)
                        }
                      }}
                    />
                      <FaEdit
                        className='faEdit'
                        onClick={() => {history.push(`/EditPortfolio/${name._id}`)}}
                      />
                  </td>
                </tr>
              )} 
            </tbody>
          </Table>
        </Container>
      </div>
    </>
  )
}

export default PortfolioDetails;
