import React, { useState, useEffect } from "react";
import axios from "axios";
import { useHistory, useParams } from "react-router-dom";
import { FaTimes } from 'react-icons/fa';

const EditUsers = () => {

  let history = useHistory(); //The useHistory hook gives you access to the history instance that you may use to navigate.
  const { id } = useParams();  //The useParams() hook helps us to access the URL parameters from a current route. 
  

  const [user ,setUser] = useState({
      name:"",
      email:"",
      username:"",
      password:"",
      role:"",
  })


  const { name, email, username, password, role } = user;

  const onInputChange = e => {
    setUser({ ...user,[e.target.name]: e.target.value });
  };

  useEffect(() => {
    loadUser();
  }, []);

  const updateUser = e => {
    e.preventDefault();
    axios.put(`http://localhost:3001/user/edit/${id}`, user);
    alert('User Updated');
    history.push("/Users");
    
  };

  const loadUser =  () => {
    axios.get(`http://localhost:3001/user/${id}`,{
            method: "GET",
          })
            .then((response) => {
              setUser({
                    id: response.data._id,
                    update: true,
                    name: response.data.name,
                    email: response.data.email,
                    username: response.data.username,
                    password: response.data.password,
                    role: response.data.role,
                }); 
            }).catch((error) => console.log("error", error));
  };

  return (
    <div className="container editP editUser">
      <div className="row mt-4"> 
        <div className="col-sm-8 col-offset-3 mx-auto shadow p-5">
          <div className="closeButtonEdit">
            <FaTimes 
              className='closeUserEdit'
              onClick={() => history.push('/Users')}
            />
          </div>
          <h4 className="text-center mb-4">Edit User</h4>
          <h5 className="text-success">User ID : {user.id} </h5>
          <div className="form-group mb-3">
            <input
              type="text"
              className="form-control form-control-lg"
              name="name"
              value={name}
              onChange={e => onInputChange(e)}
            />
          </div>
          <div className="form-group mb-3">
            <input
              type="text"
              className="form-control form-control-lg"
              name="email"
              value={email}
              onChange={e => onInputChange(e)}
            />
          </div>
          <div className="form-group mb-3">
            <input
              type="text"
              className="form-control form-control-lg"
              name="username"
              value={username}
              onChange={e => onInputChange(e)}
            />
          </div>
          <div className="form-group mb-3">
            <input
              type="password"
              className="form-control form-control-lg"
              name="password"
              value={password}
              onChange={e => onInputChange(e)}
            />
          </div>
          <div className="form-group mb-3">
            <input
              type="text"
              className="form-control form-control-lg"
              name="role"
              value={role}
              disabled
            />
          </div>
          <button 
            onClick={updateUser} 
            className="btn btn-secondary btn-block"
          >
            Update User
          </button>
        </div>
      </div> 
    </div>
  );
};

export default EditUsers;