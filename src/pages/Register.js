import React, { useState } from 'react'
import { Formik, Form, Field, ErrorMessage } from "formik";
import { FaTimes } from 'react-icons/fa';
import * as Yup from "yup";
import axios from 'axios';
import './Register.css'


function Register(props) {

	const [name, setName] = useState('');
	const [email, setEmail] = useState('');
	const [username, setUsername] = useState('');
	const [password, setPassword] = useState('');


	const initialValues = {
		name: "",
		email: "",
		username: "",
		password: "",
	};
	
	const validationSchema = Yup.object().shape({
		name: Yup.string().required(),
		email: Yup.string().email().required(),
		username: Yup.string().min(3).max(15).required(),
		password: Yup.string().min(4).max(20).required(),
	});
	
	const onSubmit = async (e) => {
		await axios.post("http://localhost:3001/user/register", {
			name: name,
			email: email,
			username: username,
			password: password
		}).then((data) => {
			if (data.status === 200){
				alert('Registration Successful')
				props.setTrigger(false)
			}
		});
	};

	const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            e.preventDefault();
            onSubmit();
        }
    }
	
	return (props.trigger) ? (
		<div className="registerform">
			<div className='popup-inner'>
				<FaTimes 
					className='close'
					onClick={() => props.setTrigger(false)}
				/>
				<Formik
					initialValues={initialValues}
					onSubmit={onSubmit}
					validationSchema={validationSchema}
				>
					<Form className="formContainer">
					<label>Name: </label>
						<ErrorMessage className="err" name="name" component="span" />
						<Field
							className="input"
							autoFocus={true}
							autoComplete="off"
							onKeyUp={(e) => { setName(e.target.value) }}
							id="name"
							name="name"
							placeholder="(Ex. John...)"
						/>
					<label>Email: </label>
						<ErrorMessage className="err" name="email" component="span" />
						<Field
							className="input"
							autoComplete="off"
							onKeyUp={(e) => { setEmail(e.target.value) }}
							id="email"
							name="email"
							placeholder="(Ex. John@gmail.com)"
						/>
						<label>Username: </label>
						<ErrorMessage className="err" name="username" component="span" />
						<Field
							className="input"
							autoComplete="off"
							onKeyUp={(e) => { setUsername(e.target.value) }}
							id="inputCreatePost"
							name="username"
							placeholder="(Ex. John123...)"
						/>
						<label>Password: </label>
						<ErrorMessage className="err" name="password" component="span" />
						<Field
							className="input"
							autoComplete="off"
							onKeyUp={(e) => { setPassword(e.target.value) }}
							type="password"
							id="inputCreatePost"
							name="password"
							placeholder="Your Password..."
							onKeyDown={handleKeyDown}
						/>
						<button 
							className="btnsubmit" 
							type="submit"
						> 
							Register
						</button>
					</Form>
				</Formik>
			</div>
			{props.children}
		</div>
	) : "";
}

export default Register