import React, { useState } from 'react';
import FileBase64 from 'react-file-base64';
import axios from 'axios';
import { FaTimes } from 'react-icons/fa';

function AddPortfolio(props) {

    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [image, setImage] = useState('');
    const [git, setGit] = useState('');

    const resetForm = () => {
        setTitle("")
        setDescription("")
        setImage("")
        setGit("")
    }

    const submitPortfolio = async (e) => {
        e.preventDefault()
        
        await axios.post("http://localhost:3001/portfolio", {
            title: title,
            description: description,
            image: image,
            git: git,
        });
        alert('Portfolio Created');
        resetForm();
        props.setTrigger(false)
    };

    return (props.trigger) ? (
        <div className="box p-3 mb-3 mt-5" style={{border:"1px solid #d0d0d0"}}>
            <div className='closeButtonEdit'>
                <FaTimes 
                    className='closeAddRecord'
                    onClick={() => props.setTrigger(false)}
                />
            </div>
            <form className='addForm'> 
                <h5 className="mb-3 ">Create New Profile Record</h5>
                <div className="form-group">
                    <input 
                        type="text" 
                        autoFocus={true}
                        className="form-control  mb-4" 
                        name="title"
                        value={title}
                        onChange={(e) => { setTitle(e.target.value) }}
                        placeholder="Enter title" 
                        required=""
                    />
                </div>
                <div className="form-group">
                    <input 
                        type="text" 
                        className="form-control  mb-4" 
                        name="description" 
                        value={description}
                        onChange={(e) => { setDescription(e.target.value) }}
                        placeholder="Enter Description" 
                        required=""
                    />
                </div>
                <div className="form-group">
                    <input 
                        type="text" 
                        className="form-control  mb-4" 
                        name="git" 
                        value={git}
                        onChange={(e) => { setGit(e.target.value) }}
                        placeholder="Enter Git Link" 
                        required=""
                    />
                </div>
                <div className="form-group">
                    <FileBase64
                        type="file"
                        multiple={false}
                        onDone={({ base64 }) => setImage(base64)}
                    />
                </div>
                <button type="submit" onClick={submitPortfolio} className="btn btn-primary btn-block mt-4">Insert Record</button>
            </form>
            {props.children}
        </div>
    ) : "";
}

export default AddPortfolio