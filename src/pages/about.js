import React from 'react';
import { Button } from 'reactstrap'
import Me from './images/EmilyAndI.jpg'
  
const About = () => {
  return (
    <div className='aboutMain'>
        <div className='inner'>
          <div className='picture'>
            <img className="img" src={Me} alt="" />
          </div>
          <div className='about'>
            <img className="img2" src={Me} alt="" />
            <h1 className="aboutTitle">About Me</h1>
            <h5>I have started University in 2003 taking Business Admin at York. After 2 years in the program I got in a serious car accident. 
              Only 7 years after the accident I was able to go back to school.</h5>
            <h5>Once graduated, only 2 years later, I committed myself to finding work that will accomodate both my physical disability 
              and work salary interests. I have searched for almost a year, before my newly wedded wife asked me what do I think about 
              owning a restaurant. That's when we decided to leave the big city and start our own family restaurant.</h5>
            <h5>However, at the beginning of 2020, COVID-19 hit us all, and I started realizing that I need to have some 
              back-up to my restaurant. So, I decided to go back to school, but this time change 
              direction and try something with computers. And that's how I got here.</h5>
            <h5>My goal now is to have fun and succeed, and take care of my family. </h5>
            <Button color="primary" href="/contact">Contact Me</Button>
          </div>
        </div>
    </div>
  );
};
  
export default About;