import React, { useState, useEffect } from "react";
import { useParams, Link } from "react-router-dom";

const EditPortfolio = () => {

  const { id } = useParams();  //The useParams() hook helps us to access the URL parameters from a current route. 
  

  const [user ,setUser] = useState({
      email:"",
      phone:"",
      name:"",
      message:"",
  })


  const { email, phone, name, message } = user;

  // useEffect(() => {
  //   loadUser();
  // }, []);


  // const loadUser =  () => {
  //   fetch(`http://localhost:5000/api/contact/${id}`,{
  //           method: "GET",
  //         })
  //           .then((response) => response.json())
  //           .then((result) => {
  //               console.log(result);
  //       setUser({
  //                   id: id,
  //                   update: true,
  //                   email: result.response[0].email,
  //                   phone: result.response[0].phone,
  //                   name: result.response[0].name,
  //                   message: result.response[0].message,
  //               });
  //           })
  //           .catch((error) => console.log("error", error));
  // };

  return (
    <div className="container">
      <div className="row mt-4"> 
        <div className="col-sm-5 col-offset-3 mx-auto shadow p-5">
          <h4 className="text-center mb-4">Contact Message</h4>

            <h5 className="text-success">Contact ID : {user.id} </h5>
            <div className="form-group mb-3">
              <h5>{email}</h5>
            </div>
            <div className="form-group mb-3">
              <h5>{phone}</h5>
            </div>
            <div className="form-group mb-3">
              <h5>{name}</h5>
            </div>
            <div className="form-group mb-3">
              <h5>{message}</h5>
            </div>

            <Link to={"/AdminContact"} ><button className="btn btn-secondary btn-block">Return to List</button></Link>

        </div>
      </div> 
    </div>
  );
};

export default EditPortfolio;