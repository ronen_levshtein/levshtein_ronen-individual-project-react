import React, { useState, useEffect } from "react";
import axios from "axios";

const ContactMe = (props) => {

  const [user, setUser] = useState([]);

  const person = user.name
  console.log(person)

  const checkToken = async () => {
    const req = await fetch("http://localhost:3001/user/token", {
        method: "POST",
        headers: {
            'x-access-token': localStorage.getItem('token')
        },
    })

    const data = await req.json();
    if (data.status === 'OK') {
        setUser(data)
    } else {
        alert(data.err)
    }
  }

  useEffect(() => {

    const token = localStorage.getItem('token');
    if (token) {
        checkToken()
    } else {
        localStorage.removeItem('token');
    }
}, [])

  const id = localStorage.getItem('id')

  const [data, setData] = useState({
    name: "",
    email: "",
    subject: "",
    message: "",
    userId: id,
  });

  console.log(data.name)

  const onInputChange = e => {
    setData({ ...data, name:user.name, email: user.email, [e.target.name]: e.target.value});
  };

  // Submit Contact 
  const submitContact = async (e) => {
    e.preventDefault();
    e.target.reset();
    await axios.post("http://localhost:3001/contact",data);
    alert('Contact Submitted');
    
  };

  return(
    <section className="contactme">
      <div className="container1">  
        <div className="">
          <div className="">
            <div className="" style={{border:"1px solid #d0d0d0"}}>
              <form className="form" 
                onSubmit={submitContact}
              > 
                  <h5 className="mb-3 title">Contact Me</h5>
                  <div className="form-group">
                      <input type="text" className="form-control mb-4" name="subject" 
                        onChange={e => onInputChange(e)}  
                        placeholder="Subject" required=""
                      />
                  </div>
                  <div className="form-group">
                      <input type="text" className="form-control mb-4" name="message" 
                        onChange={e => onInputChange(e)}  
                        placeholder="Message" required=""
                      />
                  </div>
                  <button type="submit" className="btn btn-primary btn-block mt-4">Submit</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default ContactMe;