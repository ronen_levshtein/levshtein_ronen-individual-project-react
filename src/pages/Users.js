import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import './Users.css';
import axios from 'axios';
import { FaTrash } from 'react-icons/fa'
import { FaEdit } from 'react-icons/fa'
import { Container, Table } from 'react-bootstrap';

function Users() {

  const [users,setUsers] = useState([]);
  const [search,setSearch] =useState('');

  const loadUsers = async () =>  
    {
      fetch('http://localhost:3001/user/userslist')
          .then(function(response){
            return response.json();
          })
          .then(function(myJson) {
            setUsers(myJson);
          });
    }
    useEffect(() => {
      loadUsers();
    }, []);

    // Delete portfolio Record
    const deleteRecord = (id) =>
    {
      axios.delete(`http://localhost:3001/user/deleteuser/${id}`)
        loadUsers();
    };

    // Search Users

    useEffect(() => {
      const searchRecords = async () =>
      {
              await axios.get(`http://localhost:3001/usersearch/${search}`)
              .then((response) => {
                  if(response.data === 'No Such Record') {
                      alert('No Such Record')
                      loadUsers()
                  } else {
                      setUsers(response.data);
                  }
              });
      }
      if (search === "") {
        loadUsers()
      } else {
          searchRecords()
      }
  }, [search])
    
  return (
    <div className='usersMain'>
      <Container className='contain'>
        <h1>User List</h1>
        <div className='searchRecord'>
          <div className="userSearchMain">
            <input 
              type="text" 
              id="form1" 
              onChange={(e)=>setSearch(e.target.value)} 
              className="userSearch" 
              placeholder="Search Portfolio Here" 
              style={{backgroundColor:"#ececec"}}
            />
          </div>
        </div>
        <Table responsive bordered className='noWrap'>
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Email</th>
              <th>User Name</th>
              <th>Role</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {users.slice(0).reverse().map((user, key)=>
              <tr key={key}>
                <td>{key}</td>
                <td>{user.name}</td>
                <td>{user.email}</td>
                <td>{user.username}</td>
                <td>{user.role}</td>
                <td className="text-danger mr-2">
                  <FaTrash 
                    onClick={() => {
                      const confirmBox = window.confirm(
                        "Do you really want to delete "+ user.username
                      )
                      if (confirmBox === true) {
                        deleteRecord(user._id)
                      }
                    }}
                  />
                  <Link className=" mr-2" to={`/EditUser/${user._id}`}>
                      <FaEdit style={{ marginLeft: '.8rem' }}/>
                  </Link>
                </td>
              </tr>
            )} 
          </tbody>
        </Table>
      </Container>
    </div>
  )
}

export default Users