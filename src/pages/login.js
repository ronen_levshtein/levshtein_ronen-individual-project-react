import React, { useState } from "react";
import { createBrowserHistory } from "history";
import Register from './Register';
import './Login.css';

function Login() {

    const withRefresh = createBrowserHistory({ forceRefresh: true });

    const [ buttonPopup, setButtonPopup ] = useState(false);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const handleKeyDown = (e) => {
        if (e.key === 'Enter') {
            e.preventDefault();
            handleLogin();
        }
    }

    const handleLogin = async () => {

        const response = await fetch("http://localhost:3001/user/login", {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                email,
                password,
            })
        })

        const data = await response.json();
        console.log(data)

        if(data.token){
            localStorage.setItem('token', data.token)
            localStorage.setItem('id', data.id)
            alert('Login Successful')
            withRefresh.push({
                pathname: '/personalInfo',
            })
        } else {
            alert('Incorrect Email/Password Combination')
        }
    };

    return (
        <div className="loginContainer">
            <Register 
                    trigger={buttonPopup}
                    setTrigger={setButtonPopup}
            >
            </Register>
            <div className="loginform">
                <h1>Login</h1>
                <label>Email:</label>
                <input
                    type="text"
                    autoFocus={true}
                    name="email"
                    onChange={(e) => {
                    setEmail(e.target.value);
                    }}
                />
                <label>Password:</label>
                <input
                    type="password"
                    name="password"
                    onChange={(e) => {
                    setPassword(e.target.value);
                    }}
                    onKeyDown={handleKeyDown}
                />
                <div className="registerDiv">
                    <p>Not yet Registered? Click</p>
                    <button 
                        className="regButton"
                        onClick={() => setButtonPopup(true)}
                    >
                        Here
                    </button>
                </div>
                <button 
                    onClick={handleLogin}
                > 
                    Login 
                </button>
            </div>
        </div>
    );
}

export default Login
