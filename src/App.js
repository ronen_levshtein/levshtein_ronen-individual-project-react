import React, { useState } from 'react';
import './App.css';
import { BrowserRouter as Router, Route, Switch} from "react-router-dom";
import AdminPortfolio from './pages/AdminPortfolio';
import EditPortfolio from './pages/EditPortfolio';
import EditUser from './pages/EditUsers';
// import EditContact from './pages/EditContact';
import Navbar from './components/Navbar/Navbar';
import Home from './pages/home';
import About from './pages/about';
import Portfolio from './pages/portfolio';
import AdminPage from './pages/AdminPage';
import PersonalInfo from './pages/PersonalInfo';
import Contact from './pages/contactMe';
import AdminContact from './pages/AdminContact';
// import Register from './pages/Register';
import Login from './pages/login';
import { AuthContext } from "./helpers/AuthContext";
import Users from './pages/Users';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css'


function App() {

  const [authState, setAuthState] = useState(false);

  return (
    <>
    
      <AuthContext.Provider value={{ authState, setAuthState }}>
        <Router>
          <Navbar />
            <div className="App">
              <Switch>
                <Route exact path='/' component={Home} />
                <Route path='/home' component={Home} />
                <Route path='/about' component={About} />
                <Route path='/portfolio' component={Portfolio} />
                <Route path='/AdminPage' component={AdminPage} />
                <Route path="/AdminPortfolio" component={AdminPortfolio} />
                <Route path="/EditPortfolio/:id" component={EditPortfolio} />
                <Route path="/EditUser/:id" component={EditUser} />
                <Route path="/Users" component={Users} />
                <Route path="/personalInfo" component={PersonalInfo} />
                {/* <Route path="/adminContact/edit/:id" component={EditContact} /> */}
                <Route path='/contact' component={Contact} />
                <Route path='/adminContact' component={AdminContact} />
                {/* <Route path='/sign-up' component={Register} /> */}
                <Route path='/login' component={Login} />
                {/* <Route path="/Main" exact render={(props) => <Main />} /> */}
              </Switch>
            </div>
        </Router>
      </AuthContext.Provider>
    </>
  );
}

export default App;