export default function validateInfo(values) {
    let errors = {}

    //Email
    if(!values.email) {
        errors.email = "Email required"
    } else if (!/^[A-Z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$/i.test(values.email)) {
        errors.email = "Email address is invalid"
    }

    //name
    if(!values.name.trim()) {
        errors.name = "Full name is required"
    }


    return errors;
}