import React, { useState, useEffect } from 'react';
import PasswordChange from './PasswordChange';

function PersonalInfo() {

    const [data, setData] = useState([]);
    const [ buttonPopup, setButtonPopup ] = useState(false);

    const checkToken = async () => {
        const req = await fetch("http://localhost:3001/user/token", {
            method: "POST",
            headers: {
                'x-access-token': localStorage.getItem('token')
            },
        })

        const data = await req.json();
        if (data.status === 'OK') {
            setData(data)
        } else {
            alert(data.err)
        }
    }

    console.log(data.email)

    useEffect(() => {

        const token = localStorage.getItem('token');
        if (token) {
            checkToken()
        } else {
            localStorage.removeItem('token');
        }
    }, [])

    return (
        <div className='pi'>
            <div>
                <PasswordChange 
                    trigger={buttonPopup}
                    setTrigger={setButtonPopup}
                    id={data._id}
                    email={data.email}
                />
            </div>
            <div>
                <h1>Personal Info</h1>
                <h2>Name: {data.name}</h2>
                <h2>Email: {data.email}</h2>
                <h2>User Name: {data.username}</h2>
                <button className='passwordChange' onClick={() => setButtonPopup(true)}>Change Password</button>
            </div>
        </div>
    )
}

export default PersonalInfo