import 'bootstrap/dist/css/bootstrap.min.css';
import React, { useState, useEffect } from "react";
import axios from "axios";
import { FaTrash } from 'react-icons/fa'
import { Container, Table } from 'react-bootstrap';

function AdminContact() {

    const [record, setRecord] = useState([]);
    const [search,setSearch] =useState('');
    const [selected, setSelected] = useState([]);

    // On Page load display all records 
    const loadPortfolioDetails = async () =>  
    {
        fetch('http://localhost:3001/contactlist')
            .then(function(response){
                return response.json();
            })
            .then(function(myJson) {
                setRecord(myJson);
            });
    }
    useEffect(() => {
        if(search === "") {
            loadPortfolioDetails();
        }
    }, [search]);

    const deleteRecord = (contactId) =>
    {
        axios.delete(`http://localhost:3001/deletecontact/${contactId}`)
        .then((result)=>{
            loadPortfolioDetails();
        })
        .catch(()=>{
            alert('Error in the Code');
        });
    };

    const toggleSelected = (id) => (e) => {
        setSelected((selected) => ({
            ...selected,
            [id]: !selected[id]
        }))
    };

    useEffect(() => {
        const searchRecords = async () =>
        {
                await axios.get(`http://localhost:3001/contactsearch/${search}`)
                .then((response) => {
                    if(response.data === 'No Such Record') {
                        alert('No Such Record')
                        loadPortfolioDetails()
                    } else {
                        setRecord(response.data);
                    }
                });
        }
        if (search === "") {
            loadPortfolioDetails()
        } else {
            searchRecords()
        }
    }, [search])

    return (
        <div className='admincont'>
            <h1 className="admincontTitle">ADMIN CONTACT</h1>
            <div className="input-group mb-4 mt-3">
                <div className="userSearchMain">
                    <input 
                        type="text" 
                        id="form1" 
                        name='search'
                        onChange={(e) => setSearch(e.target.value)} 
                        className="userSearch" 
                        placeholder="Search Contact Here" 
                        style={{backgroundColor:"#ececec"}}
                    />
                </div>
            </div>  
            <Container className='contain1'>
                <Table responsive bordered className='noWrap'>
                    <thead>
                    <tr>
                        <th></th>
                        <th>#</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Subject</th>
                        <th>Message</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                        {record.map((name, key)=>
                            <tr key={key}>
                                <td>
                                    <input 
                                        type='checkbox'
                                        checked={selected[name._id]}
                                        onChange={toggleSelected(name._id)}
                                    />
                                </td>
                                <td>{key + 1}</td>
                                <td>{name.name}</td>
                                <td>{name.email}</td>
                                <td>{name.subject}</td>
                                <td>{name.message}</td>
                                <td className="text-danger mr-2">
                                    {selected[name._id] === true &&
                                        <FaTrash 
                                            onClick={() => {
                                                const confirmBox = window.confirm(
                                                    "Do you really want to delete "+ name.name
                                                )
                                                if (confirmBox === true) {
                                                    deleteRecord(name._id)
                                                }
                                                }}
                                        />
                                    }
                                </td>
                            </tr>
                        )} 
                    </tbody>
                </Table>
            </Container>
        </div>
    )
}

export default AdminContact
